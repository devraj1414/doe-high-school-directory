//
//  FileReader.swift
//  DOE-High-School-Directory
//
//  Created by Rajkumar Singh on 9/29/23.
//

import Foundation

func loadJson<T: Decodable>(filename fileName: String) -> T? {
    if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
        do {
            let data = try Data(contentsOf: url)
            let decoder = JSONDecoder()
            let jsonData = try decoder.decode(T.self, from: data)
            return jsonData
        } catch {
            print("error:\(error)")
        }
    }
    return nil
}

