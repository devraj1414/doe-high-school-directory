//
//  TestSchoolsModel.swift
//  DOE-High-School-DirectoryTests
//
//  Created by Rajkumar Singh on 9/29/23.
//

import XCTest
import Combine
@testable import DOE_High_School_Directory
final class TestSchoolsModel: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSchools(){
        guard let schools: [School] = loadJson(filename: "schoolsdata") else {
            XCTFail("failed to load JSON File")
            return
        }
        XCTAssertNotNil(schools)
        XCTAssertEqual(schools.count, 440)
        XCTAssertEqual(schools[0].schoolName, "Clinton School Writers & Artists, M.S. 260")
        XCTAssertTrue(type(of: schools[0].location) == String.self)
    }
    
    func testSchoolsDetails(){
        guard let schoolsDetails : [SchoolDetail] = loadJson(filename: "schoolsdetails") else {
            XCTFail("failed to load JSON File")
            return
        }
        XCTAssertNotNil(schoolsDetails)
        XCTAssertEqual(schoolsDetails.count, 478)
        XCTAssertEqual(schoolsDetails[0].dbn, "01M292")
        XCTAssertTrue(type(of: schoolsDetails[0].schoolName) == String.self)
    }
    
    
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        // Any test you write for XCTest can be annotated as throws and async.
        // Mark your test throws to produce an unexpected failure when your test encounters an uncaught error.
        // Mark your test async to allow awaiting for asynchronous code to complete. Check the results with assertions afterwards.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
}
