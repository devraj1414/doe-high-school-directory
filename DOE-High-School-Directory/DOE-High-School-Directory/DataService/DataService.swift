//
//  DataService.swift
//  DOE-High-School-Directory
//
//  Created by Rajkumar Singh on 9/28/23.
//

import Foundation
import Combine

enum DataServiceError: Error {
    case noDataFoundError(Error)
    case invalidDataFormat(Error)
}

protocol DataServiceProtocol{
    func fetchData<T: Decodable>(url: URL)-> AnyPublisher<T, Error>
}
class DataSrvice: DataServiceProtocol{
    func fetchData<T>(url: URL) -> AnyPublisher<T, Error> where T : Decodable {
        URLSession.shared.dataTaskPublisher(for: url)
            .mapError { err in
                return DataServiceError.noDataFoundError(err)
            }
            .map { response in
                response.data
            }
            .tryMap { data in
                do{
                    return try JSONDecoder().decode(T.self, from: data)
                }
                catch(let error){
                    throw DataServiceError.invalidDataFormat(error)
                }
            }
            .eraseToAnyPublisher()
    }
}
