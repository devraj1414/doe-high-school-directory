//
//  School.swift
//  DOE-High-School-Directory
//
//  Created by Rajkumar Singh on 9/28/23.
//

import Foundation
struct School: Decodable {
    let dbn, schoolName, boro, overviewParagraph: String
    let location, phoneNumber: String
    let website: String
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case boro
        case overviewParagraph = "overview_paragraph"
        case location
        case phoneNumber = "phone_number"
        case website
    }
}
