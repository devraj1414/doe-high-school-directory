//
//  SchoolCellTableViewCell.swift
//  DOE-High-School-Directory
//
//  Created by Rajkumar Singh on 9/28/23.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {

    @IBOutlet weak var namelabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var phoneNoLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(data: School){
        namelabel.text = data.schoolName
        locationLabel.text = data.location
        phoneNoLabel.text = data.phoneNumber
    }

}
