//
//  SchoolDetailsViewModel.swift
//  DOE-High-School-Directory
//
//  Created by Rajkumar Singh on 9/28/23.
//

import Foundation
import Combine
class SchoolDetailsViewModel{
    let dataService: DataServiceProtocol
    init(dataService: DataServiceProtocol = DataSrvice()) {
        self.dataService = dataService
    }
    let schoolDetailsOutput: PassthroughSubject<SchoolsDetailsOutput, Never> = .init()
    private var store = Set<AnyCancellable>()
    enum SchoolsDetailsOutput{
        case onFetchSuccess(schoolDeatils: [SchoolDetail])
        case getSchoolDetails(school: SchoolDetail)
        case onFetchFailure(error: Error)
    }
    enum SelectedSchoolInput{
        case onSchoolSelection(School)
    }
    
    func fetchSchoolDetails(school: School){
        let url = SchoolEndpoint.schoolDeatilsEndpoint.url
        dataService.fetchData(url: url)
            .sink { [weak self] completion in
                guard let self = self else {return}
                switch completion {
                case .finished:
                    print("Finished")
                case .failure(let error):
                    self.schoolDetailsOutput.send(.onFetchFailure(error: error))
                }
            }
            receiveValue: { result in
                self.schoolDetailsOutput
                    .send(.onFetchSuccess(schoolDeatils: result))
            }
            .store(in: &store)
    }
    
    func transformInput(selectedSchol: AnyPublisher<SelectedSchoolInput, Never>) -> AnyPublisher<SchoolsDetailsOutput, Never>{
        selectedSchol.sink { [weak self] completion in
            guard let self = self else {return}
            switch completion{
            case .onSchoolSelection(let school):
                self.fetchSchoolDetails(school: school)
            }
        }
        .store(in: &store)
        return schoolDetailsOutput.eraseToAnyPublisher()
    }
}
