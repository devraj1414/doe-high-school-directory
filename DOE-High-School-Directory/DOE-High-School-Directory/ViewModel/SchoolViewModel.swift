//
//  SchoolViewModel.swift
//  DOE-High-School-Directory
//
//  Created by Rajkumar Singh on 9/28/23.
//

import Foundation
import Combine
class SchoolViewModel{
    let dataService: DataServiceProtocol
    init(dataService: DataServiceProtocol = DataSrvice() ) {
        self.dataService = dataService
    }
    private var store = Set<AnyCancellable>()
    let schoolsOutput = PassthroughSubject<SchoolsOutput, Never>()
    enum SchoolsOutput{
        case onFetchComplete
        case onFetchSuccess(schools: [School])
        case onFetchFailure(error: Error)
    }
    enum RequestInput{
        case viewDidAppear
    }
    func fetchSchoolsData(){
        let url = SchoolEndpoint.schoolsEndPoint.url
        dataService.fetchData(url: url)
            .sink { [weak self] completion in
                guard let self = self else {return}
                switch completion {
                case .finished:
                    print("Finished")
                    self.schoolsOutput.send(.onFetchComplete)
                case .failure(let error):
                    self.schoolsOutput.send(.onFetchFailure(error: error))
                }
            } receiveValue: { result in
                self.schoolsOutput.send(.onFetchSuccess(schools: result))
            }
            .store(in: &store)
    }
    
    func transformRequest(input: AnyPublisher<RequestInput, Never>) -> AnyPublisher<SchoolsOutput, Never>{
        input.sink { [weak self] input in
            guard let self = self else {return}
            switch input {
            case .viewDidAppear:
                self.fetchSchoolsData()
            }
        }
        .store(in: &store)
        return schoolsOutput.eraseToAnyPublisher()
    }
}
