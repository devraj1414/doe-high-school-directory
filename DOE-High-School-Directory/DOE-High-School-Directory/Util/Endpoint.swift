//
//  Endpoint.swift
//  DOE-High-School-Directory
//
//  Created by Rajkumar Singh on 9/29/23.
//

import Foundation
private extension URL {
    static func makeSchoolEndpoint(_ endpoint: String) -> URL {
        URL(string: "https://data.cityofnewyork.us/\(endpoint)")!
    }
}
// MARK: - Movies Endpoint
enum SchoolEndpoint {
    case schoolsEndPoint
    case schoolDeatilsEndpoint
    case hostEndPoint
}
//Extension to Generate Endpoint URL
extension SchoolEndpoint {
    var url: URL {
        switch self {
        case .schoolsEndPoint:
            return .makeSchoolEndpoint("resource/s3k6-pzi2.json")
        case .schoolDeatilsEndpoint:
            return .makeSchoolEndpoint("resource/f9bf-2cp4.json")
        case .hostEndPoint:
            return.makeSchoolEndpoint("")
        }
    }
}
