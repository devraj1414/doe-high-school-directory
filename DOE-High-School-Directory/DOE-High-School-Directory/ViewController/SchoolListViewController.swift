//
//  SchoolListViewController.swift
//  DOE-High-School-Directory
//
//  Created by Rajkumar Singh on 9/28/23.
//

import UIKit
import Combine
class SchoolListViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    let viewModel: SchoolViewModel = SchoolViewModel()
    var selectedSchool: School?
    private var store = Set<AnyCancellable>()
    @Published var schools: [School] = []
    @Published var isLoading: Bool = false
    let inputRequest: PassthroughSubject<SchoolViewModel.RequestInput, Never> = .init()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "NYC Schools"
        bind()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        activityIndicatorView.startAnimating()
       // activityIndicatorView.hidesWhenStopped
        inputRequest.send(.viewDidAppear)
    }
    private func bind(){
        viewModel.transformRequest(input: inputRequest.eraseToAnyPublisher())
            .sink {[weak self] output in
                guard let self = self else {return}
                switch output{
                case .onFetchComplete:
                    self.isLoading = false
                case .onFetchSuccess(let schools):
                    self.schools = schools
                case .onFetchFailure(let error):
                    self.presentAlert(title: "Error", message: error.localizedDescription)
                }
            }
            .store(in: &store)
        $schools
            .receive(on: DispatchQueue.main)
            .sink {[weak self] schools in
                guard let self = self else {return}
                self.tableView.reloadData()
            }
            .store(in: &store)
        $isLoading
            .receive(on: DispatchQueue.main)
            .sink {[weak self] isLoading in
            guard let self = self else {return}
                self.hideLoader()
            }
            .store(in: &store)
    }
    private func hideLoader(){
        activityIndicatorView.stopAnimating()
        activityIndicatorView.isHidden = true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SchoolListViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schools.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? SchoolTableViewCell else {return UITableViewCell()}
        let school = schools[indexPath.row]
        cell.configure(data: school)
                return cell
    }
  
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         selectedSchool = schools[indexPath.row]
        guard let schoolDetails = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "schoolDeatils") as? SchoolDetailsViewController else {return}
        schoolDetails.selectedSchool = selectedSchool
        self.navigationController?.pushViewController(schoolDetails, animated: true)
            
    }
}

extension UIViewController {
    public func presentAlert(
          title: String,
          message: String)
      {
          let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
          
               let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                   print("Ok button tapped");
               }
               
               alertController.addAction(OKAction)
          present(alertController, animated: true, completion: nil)
      }
      
}


