//
//  SchoolDetailsViewController.swift
//  DOE-High-School-Directory
//
//  Created by Rajkumar Singh on 9/29/23.
//

import UIKit
import Combine
class SchoolDetailsViewController: UIViewController {
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var noOfTestTakersLabel: UILabel!
    @IBOutlet weak var criticalReadingScoreLabel: UILabel!
    @IBOutlet weak var mathAvgScoreLabel: UILabel!
    @IBOutlet weak var writingAvgScorelabel: UILabel!
    
    
    let viewModel: SchoolDetailsViewModel = SchoolDetailsViewModel()
    private var store = Set<AnyCancellable>()
    var selectedSchool: School?
    let selelectedSchool: PassthroughSubject<String, Never> = .init()
    @Published var schoolDetails: SchoolDetail?
    let inputRequest: PassthroughSubject<SchoolDetailsViewModel.SelectedSchoolInput, Never> = .init()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Schools Details"
        bind()
    }
    override func viewDidAppear(_ animated: Bool) {
        guard let sc = selectedSchool else {return}
        inputRequest.send(.onSchoolSelection(sc))
    }
    
    func bind(){
        viewModel.transformInput(selectedSchol: inputRequest.eraseToAnyPublisher())
            .receive(on: DispatchQueue.main)
            .sink {[weak self] output in
                guard let self = self else {return}
                switch output{
                case .onFetchSuccess(let schools):
                    schoolDetails = schools.first{$0.dbn == self.selectedSchool?.dbn}
                    guard let details = schoolDetails else {
                        self.presentAlert(title: "No Data Found", message: "Details of School are not Available")
                        return
                    }
                case .onFetchFailure(let error):
                    self.presentAlert(title: "Error", message: error.localizedDescription)
                case .getSchoolDetails(school: let school):
                    print(school)
                }
            }
            .store(in: &store)
        
        $schoolDetails
            .receive(on: DispatchQueue.main)
            .sink {[weak self] details in
                guard let self = self else {return}
                self.showSchoolSdetails()
            }
            .store(in: &store)
        
    }
    
    private func showSchoolSdetails(){
        guard let details = schoolDetails else {
            return
        }
        schoolNameLabel.text = details.schoolName
        noOfTestTakersLabel.text = "Total test taker:   \(details.numOfSatTestTakers)"
        criticalReadingScoreLabel.text = "Reading Average Score:   \(details.satCriticalReadingAvgScore)"
        mathAvgScoreLabel.text = "Maths Average Score:    \(details.satMathAvgScore)"
        writingAvgScorelabel.text = "Writing Average Score:   \(details.satMathAvgScore)"
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UITextView {
    func hyperLink(originalText: String, hyperLink: String, urlString: String) {
        let style = NSMutableParagraphStyle()
        style.alignment = .center
        let attributedOriginalText = NSMutableAttributedString(string: originalText)
        let linkRange = attributedOriginalText.mutableString.range(of: hyperLink)
        let fullRange = NSMakeRange(0, attributedOriginalText.length)
        self.attributedText = attributedOriginalText
    }
}
